#!/bin/bash 
sudo apt-get update
echo "*********************************************************packages got updated*********************************************************"

sudo apt-get install -y apache2
echo "*********************************************************apache2 got isntalled*********************************************************"

sudo apt-get install -y mysql-server
echo "*********************************************************mysql server got installed*********************************************************"

sudo mysql <<EOF
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
EOF
echo "*********************************************************altered root password*********************************************************"

sudo service mysql stop
echo "*********************************************************stopped mysql*********************************************************"

sudo service mysql start
echo "*********************************************************stated mysql*********************************************************"

mysql -uroot -p'root' <<EOF
create database zabbix character set utf8 collate utf8_bin;
grant all privileges on zabbix.* to zabbix@localhost identified by 'zabbix';
flush privileges;
EOF
echo "*********************************************************create database with the name zabbix and updated the password and given the privileges*********************************************************"

sudo apt-get -y install php php-pear php-cgi php-common libapache2-mod-php php-mbstring php-net-socket php-gd php-xml-util php-mysql php-gettext php-bcmath
echo "*********************************************************installed php*********************************************************"

sudo service apache2 restart
echo "*********************************************************restarted apache2*********************************************************"

sudo wget https://repo.zabbix.com/zabbix/4.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.4-1+bionic_all.deb
echo "*********************************************************downnloaded zabbix package*********************************************************"

sudo dpkg -i zabbix-release_4.4-1+bionic_all.deb
echo "*********************************************************unpacked zabbix package*********************************************************"

sudo apt-get update

sudo apt install -y zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent
echo "*********************************************************installed zabbix-server-mysql zabbix-frontend-php zabbix-apache-conf zabbix-agent*********************************************************"

sudo zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -uzabbix -p zabbix
echo "*********************************************************zcat*********************************************************"

sudo chmod o+w /etc/zabbix/zabbix_server.conf
echo "*********************************************************given root permissions*******************************************************"


sudo cat <<EOF >> /etc/zabbix/zabbix_server.conf
DBHost=localhost
DBPassword=zabbix
EOF
echo "*********************************************************makde modifications in zabbix_server.conf file*********************************************************"

sudo chmod o-w /etc/zabbix/zabbix_server.conf
echo "*********************************************************removed root permissions*******************************************************"


sudo sed -i 's+# php_value date.timezone Europe/Riga+php_value date.timezone asia/kolkata+g' /etc/zabbix/apache.conf

sudo systemctl restart apache2
echo "*********************************************************restarted apache2*********************************************************"

sudo systemctl start zabbix-server
echo "*********************************************************start zabbix-server*********************************************************"

sudo systemctl enable zabbix-server
echo "*********************************************************enable zabbix-server*********************************************************"