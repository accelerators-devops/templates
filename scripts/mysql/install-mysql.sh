sudo apt-get update

sudo apt-get install -y mysql-server

sudo mysql <<EOF
CREATE USER 'zbx_monitor'@'%' IDENTIFIED BY 'zabbix';
GRANT USAGE,REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zbx_monitor'@'%';
EOF

echo "************************************mysql got installed and configured*****************************"

sudo mkdir /var/lib/zabbix

sudo touch /var/lib/zabbix/.my.cnf

sudo chmod o+w /var/lib/zabbix/.my.cnf

sudo cat <<EOF >> /var/lib/zabbix/.my.cnf
[client]
user=zbx_monitor
password=zabbix
[mysql]
user=zbx_monitor
password=zabbix
[mysqladmin]
user=zbx_monitor
password=zabbix
EOF

sudo chmod o-w /var/lib/zabbix/.my.cnf


sudo sed -i '15,18 s/^/#/' /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf

sudo chmod o+w /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf

sudo cat <<EOF >> /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf
UserParameter=mysql.ping[*], mysqladmin -h"$1" -P"$2" ping
UserParameter=mysql.get_status_variables[*], mysql -h"$1" -P"$2" -sNX -e "show global status"
UserParameter=mysql.version[*], mysqladmin -s -h"$1" -P"$2" version
UserParameter=mysql.db.discovery[*], mysql -h"$1" -P"$2" -sN -e "show databases"
UserParameter=mysql.dbsize[*], mysql -h"$1" -P"$2" -sN -e "SELECT SUM(DATA_LENGTH + INDEX_LENGTH) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='$3'"
UserParameter=mysql.replication.discovery[*], mysql -h"$1" -P"$2" -sNX -e "show slave status"
UserParameter=mysql.slave_status[*], mysql -h"$1" -P"$2" -sNX -e "show slave status"
EOF

sudo chmod o-w /etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf

sudo systemctl restart zabbix-agent
sudo systemctl enable zabbix-agent