#!/bin/bash 
sudo apt-get update
echo "*********************************************************packages got updated*********************************************************"

sudo apt-get install -y default-jdk
echo "*********************************************************JDK got installed*********************************************************"

sudo chmod o+w ~/.bashrc
sudo cat <<EOF >> ~/.bashrc
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export PATH=$PATH:$JAVA_HOME/bin
EOF
sudo chmod o-w ~/.bashrc

echo "*********************************************************java home is configured********"

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
echo "*********************************************************deb http://apt.postgresql.org/pub/repos/apt/*********************************************************"

wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
echo "*********************************************************wget*********************************************************" 

sudo apt-get update -y
echo "*********************************************************packages got updated*********************************************************"

sudo apt-get install -y postgresql postgresql-contrib
echo "*********************************************************postgres got installed*********************************************************"

 
sudo passwd postgres  

sudo su - postgres <<EOF
createuser sqube 
psql <<EOFPSQL
ALTER USER sqube WITH ENCRYPTED password 'sqube';  
CREATE DATABASE sqube OWNER sqube;
\q
EOFPSQL
EOF

echo "*********************************************************created sqube user in postgres and changed the password*********************************************************"

sudo adduser sonar
echo "*********************************************************added user sonar*********************************************************"
 
wget https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.7.6.zip
echo "*********************************************************downloaded sonarqube zip file*********************************************************"

sudo apt-get install -y unzip
echo "*********************************************************installing 'unzip' package*********************************************************"

unzip sonarqube-6.7.6.zip
echo "*********************************************************unziped the zip folder*********************************************************"

sudo mkdir /opt/sonarqube
echo "********************************************************mkdir /opt/sonarqube*********************************************************"
  
sudo cp -r sonarqube-6.7.6/. /opt/sonarqube/
echo "*********************************************************copied sonar unziped folder to /opt/sonarqube folder*********************************************************"

 
sudo chown -R sonar:sonar /opt/sonarqube
echo "******************************************************chown -R sonar:sonar /opt/sonarqube**********************************************************"
 
sudo chmod o+w /opt/sonarqube/bin/linux-x86-64/sonar.sh
echo "********************************************************added the write permissions of sonar.sh*********************************************************"


sudo cat <<EOF >> /opt/sonarqube/bin/linux-x86-64/sonar.sh 
RUN_AS_USER=sonar
EOF
echo "********************************************************added RUN_AS_USER in sonar.sh*********************************************************"

sudo chmod o-w /opt/sonarqube/bin/linux-x86-64/sonar.sh
echo "********************************************************removed the write permissions of sonar.sh*********************************************************"

sudo chmod o+w /opt/sonarqube/conf/sonar.properties
echo "********************************************************added the write permissions of sonar.properties*********************************************************"

sudo cat <<EOF >> /opt/sonarqube/conf/sonar.properties
sonar.jdbc.username=sqube
sonar.jdbc.password=sqube
sonar.jdbc.url=jdbc:postgresql://localhost/sqube
sonar.web.host=0.0.0.0 (If not setting up proxy, if yes give loop back ip)
sonar.web.javaAdditionalOpts=-server
EOF
echo "******************************************************made modifications in sonar.properties file**********************************************************"

sudo chmod o-w /opt/sonarqube/conf/sonar.properties
echo "********************************************************removed the write permissions of sonar.properties*********************************************************"

sudo chmod o+w /opt/sonarqube/elasticsearch/config/elasticsearch.yml
echo "********************************************************added the write permissions of elasticsearch.yml *********************************************************"


sudo cat <<EOF >> /opt/sonarqube/elasticsearch/config/elasticsearch.yml 
node.name: ${hostname}
network.host: 0.0.0.0
EOF
echo "******************************************************made modifications in elasticsearch.yml**********************************************************"

sudo chmod o-w /opt/sonarqube/elasticsearch/config/elasticsearch.yml
echo "*******************************************************removed the write permissions of elasticsearch.yml *********************************************************"

sudo touch /etc/systemd/system/sonar.service

sudo chmod o+w /etc/systemd/system/sonar.service
echo "*******************************************************addded the write permissions to sonar.service *********************************************************"


sudo cat <<EOF >> /etc/systemd/system/sonar.service
[Unit]
Description=SonarQube service
After=syslog.target network.target
[Service]
Type=forking
	
ExecStart=/opt/sonarqube/bin/linux-x86-64/sonar.sh start
ExecStop=/opt/sonarqube/bin/linux-x86-64/sonar.sh stop
User=sonar
Group=sonar
Restart=always
[Install]
WantedBy=multi-user.target 
EOF
echo "******************************************************made changes in sonar.service**********************************************************"

sudo chmod o-w /etc/systemd/system/sonar.service
echo "*******************************************************removed the write permissions to sonar.service *********************************************************"

  
sudo systemctl restart sonar
echo "******************************************************restarted sonar**********************************************************"

sudo systemctl enable sonar
echo "******************************************************enabled sonar**********************************************************"



chmod +x install-zabbix-agent.sh
./install-zabbix-agent.sh